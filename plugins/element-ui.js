import Vue from 'vue';
import {
    DatePicker,
    TimePicker,
} from 'element-ui';
import lang from 'element-ui/lib/locale/lang/ru-RU';
import locale from 'element-ui/lib/locale';

locale.use(lang);
export default () => {
    Vue.use(TimePicker, { locale });
    Vue.use(DatePicker, { locale });
};
