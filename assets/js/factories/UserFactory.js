export default {
    create(from) {
        return {
            id: null,
            firstName: null,
            lastName: null,
            middleName: null,
            email: null,
            phone: null,
            subscriptionCode: null,
            subscriptionTill: null,
            role: 'user',
            updatedAt: null,
            createdAt: null,
            ...from,
        };
    },
};
