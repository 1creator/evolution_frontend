const route = 'gallery-images';

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
};
