export const state = () => {
    return {
        items: [],
    };
};

export const mutations = {
    updateState(state, data) {
        if (!state) {
            state = {};
        }
        Object.assign(state, data);
    },
    add(state, item) {
        state.items.push(item);
    },
    update(state, { id, data }) {
        const updatingItem = state.items.find(item => item.id === id);
        if (updatingItem) {
            Object.assign(updatingItem, data);
        }
    },
    remove(state, id) {
        const index = state.items.findIndex(item => item.id === id);
        if (index >= 0) {
            state.items.splice(index, 1);
        }
    },
};

export const getters = {
    whereId: state => id => state.items.find(item => item.id === id),
};

export const actions = {
    async fetch(ctx, data) {
        const res = await this.$api.products.fetch({
            limit: 999,
        });
        ctx.commit('updateState', {
            items: res.results,
        });
    },
    async store(context, data) {
        const res = await this.$api.products.store(data);
        context.commit('add', res);
        return res;
    },
    async update(context, { id, data }) {
        const res = await this.$api.products.update(id, data);
        context.commit('update', {
            id,
            data: res,
        });
        return res;
    },
    async remove(context, id) {
        await this.$api.products.delete(id);
        context.commit('remove', id);
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
