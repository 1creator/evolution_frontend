const route = 'gallery-images';

function normalizeParams(params) {
    if (params.images) {
        params.imageIds = params.images.map(i => i.id);
        delete params.images;
    }
    return params;
}

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async update(params) {
        params = normalizeParams(params);
        return await this.$axios.put(`${route}`, params);
    },
};
