export { default as feedback } from '~/plugins/api/user/feedback';
export { default as products } from '~/plugins/api/user/products';
export { default as groups } from '~/plugins/api/user/groups';
export { default as payments } from '~/plugins/api/user/payments';
export { default as visits } from '~/plugins/api/user/visits';
export { default as promos } from '~/plugins/api/user/promos';
export { default as galleryImages } from '~/plugins/api/user/gallery-images';
export { default as schedules } from '~/plugins/api/user/schedules';
export { default as subscriptions } from '~/plugins/api/user/subscriptions';
export { default as trainers } from '~/plugins/api/user/trainers';
