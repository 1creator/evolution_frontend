const route = 'visits';

function normalizeParams(params) {
    params = JSON.parse(JSON.stringify(params));
    if (params.user) {
        params.userId = params.user.id;
        delete params.user;
    }
    return params;
}

export default {
    async fetchStats(params) {
        return await this.$axios.get(`${route}/stats`, { params });
    },
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`${route}/${id}`, { params });
    },
    async store(params) {
        params = normalizeParams(params);
        return await this.$axios.post(`${route}`, params);
    },
    async update(id, params) {
        params = normalizeParams(params);
        return await this.$axios.put(`${route}/${id}`, params);
    },
    async delete(id) {
        return await this.$axios.delete(`${route}/${id}`);
    },
};
