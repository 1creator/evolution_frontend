export function formatPaymentType(val) {
    return {
        cash: 'Наличные',
        terminal: 'Терминал',
        online: 'Онлайн оплата',
    }[val] || val;
}
