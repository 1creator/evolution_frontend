export default {
    create(from) {
        return {
            id: null,
            name: null,
            price: null,
            description: null,
            daysCount: null,
            updatedAt: null,
            createdAt: null,
            ...from,
        };
    },
};
