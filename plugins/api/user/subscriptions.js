const route = 'subscriptions';

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`${route}/${id}`, { params });
    },
    async buy(id) {
        return await this.$axios.post(`${route}/${id}/buy`);
    },
};
