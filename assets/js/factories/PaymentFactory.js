export default {
    create(from) {
        const res = {
            id: null,
            userId: null,
            productId: null,
            amount: null,
            type: 'cash',
            status: 'created',
            updatedAt: null,
            createdAt: new Date(),
            ...from,
        };
        if (from?.payment) {
            res.amount = from.payment.amount;
            res.status = from.payment.status;
            res.type = from.payment.type;
        }
        return res;
    },
};
