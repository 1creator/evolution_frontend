export const state = () => {
    return {
        items: [],
    };
};

export const mutations = {
    updateState(state, data) {
        if (!state) {
            state = {};
        }
        Object.assign(state, data);
    },
};

export const getters = {
    whereId: state => id => state.items.find(item => item.id === id),
};

export const actions = {
    async fetch(ctx, data) {
        const res = await this.$api.groups.fetch({
            limit: 999,
        });
        ctx.commit('updateState', {
            items: res.results,
        });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
