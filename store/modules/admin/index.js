export { default as products } from '~/store/modules/admin/products';
export { default as trainers } from '~/store/modules/admin/trainers';
export { default as groups } from '~/store/modules/admin/groups';
export { default as subscriptions } from '~/store/modules/admin/subscriptions';
export { default as settings } from '~/store/modules/admin/settings';
