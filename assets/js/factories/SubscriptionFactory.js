export default {
    create(from) {
        return {
            id: null,
            name: null,
            price: null,
            daysCount: null,
            useCount: null,
            description: null,
            isUnlimited: false,
            ordering: 0,
            createdAt: null,
            ...JSON.parse(JSON.stringify(from)),
        };
    },
};
