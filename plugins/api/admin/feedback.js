const route = 'feedback';

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async store(params) {
        return await this.$axios.post(`${route}`, params);
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`${route}/${id}`, { params });
    },
    async markResolved(id) {
        return await this.$axios.put(`${route}/${id}/touch`);
    },
    async delete(id) {
        return await this.$axios.delete(`${route}/${id}`);
    },
};
