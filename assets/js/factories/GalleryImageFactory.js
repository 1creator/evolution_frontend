export default {
    create(from) {
        return {
            id: null,
            updatedAt: null,
            createdAt: null,
            ...from,
        };
    },
};
