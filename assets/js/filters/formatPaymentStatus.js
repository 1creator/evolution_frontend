export function formatPaymentStatus(val) {
    return {
        created: 'Создан',
        pending: 'В процессе',
        succeeded: 'Успешно',
        failed: 'Ошибка',
        refunded: 'Возврат',
    }[val] || val;
}
