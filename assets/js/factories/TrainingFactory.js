export default {
    create(from) {
        return {
            id: null,
            name: null,
            description: null,
            active: true,
            updatedAt: null,
            createdAt: null,
            ...from,
        };
    },
};
