export const state = () => {
    return {
        general: {},
    };
};

export const mutations = {
    updateState(state, data) {
        if (!state) {
            state = {};
        }
        Object.assign(state, data);
    },
    updateGeneral(state, data) {
        state.general = data;
    },
};

export const actions = {
    async fetch(ctx) {
        try {
            const settings = await this.$api.settings.fetch();
            ctx.commit('updateState', settings.results);
        } catch (e) {
            console.error(e);
        }
    },
    async updateGeneral(ctx, data) {
        const res = await this.$api.settings.update('general', data);
        ctx.commit('updateGeneral', res.value);
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};
