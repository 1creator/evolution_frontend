const route = 'trainings';

function normalizeParams(params) {
    params = JSON.parse(JSON.stringify(params));
    if (params.image) {
        params.imageId = params.image.id;
        delete params.image;
    }
    if (params.schedules) {
        params.schedules = params.schedules.map((i) => {
            return {
                ...i,
                trainerId: i.trainer ? i.trainer.id : null,
            };
        });
    }
    return params;
}

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`${route}/${id}`, { params });
    },
    async store(params) {
        params = normalizeParams(params);
        return await this.$axios.post(`${route}`, params);
    },
    async update(id, params) {
        params = normalizeParams(params);
        return await this.$axios.put(`${route}/${id}`, params);
    },
    async delete(id) {
        return await this.$axios.delete(`${route}/${id}`);
    },
};
