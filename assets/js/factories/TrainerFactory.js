export default {
    create(from) {
        return {
            id: null,
            lastName: null,
            firstName: null,
            description: null,
            position: null,
            updatedAt: null,
            createdAt: null,
            ...from,
        };
    },
};
