const route = 'payments';

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
};
