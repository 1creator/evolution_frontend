const route = 'subscriptions';

function normalizeParams(params) {
    params = JSON.parse(JSON.stringify(params));
    if (params.user) {
        params.userId = params.user.id;
        delete params.user;
    }
    if (params.product) {
        params.productId = params.product.id;
        delete params.product;
    }
    if (params.image) {
        params.imageId = params.image.id;
        delete params.image;
    }
    if (params.trainings) {
        params.trainingIds = params.trainings.filter(i => i).map(i => i.id);
        delete params.trainings;
    }
    return params;
}

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
    async fetchOne(id, params) {
        return await this.$axios.get(`${route}/${id}`, { params });
    },
    async store(params) {
        params = normalizeParams(params);
        return await this.$axios.post(`${route}`, params);
    },
    async update(id, params) {
        params = normalizeParams(params);
        return await this.$axios.put(`${route}/${id}`, params);
    },
    async give(params) {
        params = normalizeParams(params);
        return await this.$axios.post(`${route}/give`, params);
    },
    async delete(id) {
        return await this.$axios.delete(`${route}/${id}`);
    },
    async buy(id) {
        return await this.$axios.post(`${route}/${id}/buy`);
    },
};
