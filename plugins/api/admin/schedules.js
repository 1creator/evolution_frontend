const route = 'schedules';

export default {
    async fetch(params) {
        return await this.$axios.get(`${route}`, { params });
    },
};
