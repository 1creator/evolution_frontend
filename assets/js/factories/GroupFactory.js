export default {
    create(from) {
        return {
            id: null,
            name: null,
            updatedAt: null,
            createdAt: null,
            ...JSON.parse(JSON.stringify(from)),
        };
    },
};
