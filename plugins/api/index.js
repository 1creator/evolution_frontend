import humps from 'humps';
import qs from 'qs';

export default function({ $axios, redirect, store, $cookies, $config, error }, inject) {
    // humps - библиотека для преобразования camelCase -> snake_case и наоборот

    /*
    во время запроса мы преобразовываем camelCase -> snake_case
    помимо этого, с помощью библиотеки query-string мы преобразуем Query параметры в необходимый формат (repeat)
    * */
    //
    //
    // arrayValue = [1, 2, 3, 4, 5]
    // http://api.1creator.ru/array_field=1,2,3,4,5
    // http://api.1creator.ru/array_field=str:1,2,3,4,5
    // http://api.1creator.ru/array_field=1&array_field=2&array_field=3&array_field=4&array_field=5
    // http://api.1creator.ru/array_field[]=1&array_field[]=2&array_field[]=3&array_field[]=4&array_field[]=5
    // http://api.1creator.ru/array_field[0]=1&array_field[1]=2&array_field[2]=3&array_field[3]=4&array_field[4]=5
    $axios.onRequest((config) => {
        if ('humps' in config && config.humps === false) {
            return config;
        }

        if (config.params) {
            config.params = humps.decamelizeKeys(config.params);

            config.paramsSerializer = function(params) {
                return qs.stringify(params, { arrayFormat: 'repeat' });
            };
        }

        if (config.data) {
            config.data = humps.decamelizeKeys(config.data);
        }
    });

    // при ответе от сервера, мы изменяем формат snake_case -> camelCase
    $axios.onResponse((response) => {
        if (!('humps' in response.config) || response.config.humps === true) {
            return humps.camelizeKeys(response.data);
        } else {
            return response.data;
        }
    });

    // во время ошибки запроса, мы выводим её в консоль
    // и в случае если код ошибки 401, мы логаутим пользователя
    $axios.onError((err) => {
        // eslint-disable-next-line no-console
        console.error(err);
        if (!('humps' in err.config) || err.config.humps === true) {
            err.response.data = humps.camelizeKeys(err.response.data);
        }
        if (err.response.status === 401 && !err.config.preventAuthError) {
            store.dispatch('auth/logout');
            error({
                statusCode: 401,
                message: 'Кажется, вы не авторизовались. Пожалуйста войдите в свою учетную запись.',
            });
        }
        return Promise.reject(err.response);
    });

    $axios.setBaseURL($config.apiHost);

    inject('api', {
        register(modules) {
            for (const key of Object.keys(modules)) {
                modules[key].$axios = $axios;
                this[key] = modules[key];
            }
        },
    });
}
