const route = 'feedback';

export default {
    async store(params) {
        return await this.$axios.post(`${route}`, params);
    },
};
